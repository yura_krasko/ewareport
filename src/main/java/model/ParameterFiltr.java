package model;

import java.io.Serializable;

public class ParameterFiltr<T> implements Serializable {

    private static final long serialVersionUID = 123215566L;

    private String field;
    private String comparison;
    private String type;
    private T value;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getComparison() {
        return comparison;
    }

    public void setComparison(String comparison) {
        this.comparison = comparison;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
