package model;

import java.io.Serializable;
import java.util.Map;

public class Contract implements Serializable {

    private static final long serialVersionUID = 12366566L;

    private String type;
    private int id;
    private String code;
    private SalePoint salePoint;
    private Map<String, Object> user;
    private String created;
    private String stateChanged;
    private Map<String, Object> stateChangedBy;
    private Tarif tariff;
    private String number;
    private String date;
    private String dateFrom;
    private String dateTo;
    private Map<String, Object> period;
    private Customer customer;
    private Map<String, Object> beneficiary;
    private Auto insuranceObject;
    private String insuranceObjects;
    private String baseTariff;
    private String discount;
    private String urgencyCoefficient;
    private Double payment;
    private Double brokerDiscountedPayment;
    private String agents;
    private String notes;
    private String state;
    private Double payments;
    private String risks;
    private String customFields;
    private Boolean multiObject;
    private String signerOtpState;
    private String customerOtpState;
    private String usageMonths;
    private String franchise;
    private Double k1;
    private Double k2;
    private Double k3;
    private Double k4;
    private Double k5;
    private Double k6;
    private Double bonusMalus;
    private Double  privilege;
    private String  privilegeType;
    private Boolean drivingExpLessThreeYears;
    private Boolean taxi;
    private String paymentPurpose;
    private String commission;
    private String insurerAccount;
    private String stickerNumber;
    private String attachments;

    public Contract() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SalePoint getSalePoint() {
        return salePoint;
    }

    public void setSalePoint(SalePoint salePoint) {
        this.salePoint = salePoint;
    }

    public Map<String, Object> getUser() {
        return user;
    }

    public void setUser(Map<String, Object> user) {
        this.user = user;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStateChanged() {
        return stateChanged;
    }

    public void setStateChanged(String stateChanged) {
        this.stateChanged = stateChanged;
    }

    public Map<String, Object> getStateChangedBy() {
        return stateChangedBy;
    }

    public void setStateChangedBy(Map<String, Object> stateChangedBy) {
        this.stateChangedBy = stateChangedBy;
    }

    public Tarif getTariff() {
        return tariff;
    }

    public void setTariff(Tarif tariff) {
        this.tariff = tariff;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public Map<String, Object> getPeriod() {
        return period;
    }

    public void setPeriod(Map<String, Object> period) {
        this.period = period;
    }

    public Map<String, Object> getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Map<String, Object> beneficiary) {
        this.beneficiary = beneficiary;
    }

    public Auto getInsuranceObject() {
        return insuranceObject;
    }

    public void setInsuranceObject(Auto insuranceObject) {
        this.insuranceObject = insuranceObject;
    }

    public String getInsuranceObjects() {
        return insuranceObjects;
    }

    public void setInsuranceObjects(String insuranceObjects) {
        this.insuranceObjects = insuranceObjects;
    }


    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public Double getBrokerDiscountedPayment() {
        return brokerDiscountedPayment;
    }

    public void setBrokerDiscountedPayment(Double brokerDiscountedPayment) {
        this.brokerDiscountedPayment = brokerDiscountedPayment;
    }

    public String getAgents() {
        return agents;
    }

    public void setAgents(String agents) {
        this.agents = agents;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getPayments() {
        return payments;
    }

    public void setPayments(Double payments) {
        this.payments = payments;
    }

    public String getRisks() {
        return risks;
    }

    public void setRisks(String risks) {
        this.risks = risks;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public Boolean getMultiObject() {
        return multiObject;
    }

    public void setMultiObject(Boolean multiObject) {
        this.multiObject = multiObject;
    }

    public String getSignerOtpState() {
        return signerOtpState;
    }

    public void setSignerOtpState(String signerOtpState) {
        this.signerOtpState = signerOtpState;
    }

    public String getCustomerOtpState() {
        return customerOtpState;
    }

    public void setCustomerOtpState(String customerOtpState) {
        this.customerOtpState = customerOtpState;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBaseTariff() {
        return baseTariff;
    }

    public void setBaseTariff(String baseTariff) {
        this.baseTariff = baseTariff;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getUrgencyCoefficient() {
        return urgencyCoefficient;
    }

    public void setUrgencyCoefficient(String urgencyCoefficient) {
        this.urgencyCoefficient = urgencyCoefficient;
    }

    public String getUsageMonths() {
        return usageMonths;
    }

    public void setUsageMonths(String usageMonths) {
        this.usageMonths = usageMonths;
    }

    public String getFranchise() {
        return franchise;
    }

    public void setFranchise(String franchise) {
        this.franchise = franchise;
    }

    public Double getK1() {
        return k1;
    }

    public void setK1(Double k1) {
        this.k1 = k1;
    }

    public Double getK2() {
        return k2;
    }

    public void setK2(Double k2) {
        this.k2 = k2;
    }

    public Double getK3() {
        return k3;
    }

    public void setK3(Double k3) {
        this.k3 = k3;
    }

    public Double getK4() {
        return k4;
    }

    public void setK4(Double k4) {
        this.k4 = k4;
    }

    public Double getK5() {
        return k5;
    }

    public void setK5(Double k5) {
        this.k5 = k5;
    }

    public Double getK6() {
        return k6;
    }

    public void setK6(Double k6) {
        this.k6 = k6;
    }

    public Double getBonusMalus() {
        return bonusMalus;
    }

    public void setBonusMalus(Double bonusMalus) {
        this.bonusMalus = bonusMalus;
    }

    public Double getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Double privilege) {
        this.privilege = privilege;
    }

    public String getPrivilegeType() {
        return privilegeType;
    }

    public void setPrivilegeType(String privilegeType) {
        this.privilegeType = privilegeType;
    }

    public Boolean getDrivingExpLessThreeYears() {
        return drivingExpLessThreeYears;
    }

    public void setDrivingExpLessThreeYears(Boolean drivingExpLessThreeYears) {
        this.drivingExpLessThreeYears = drivingExpLessThreeYears;
    }

    public Boolean getTaxi() {
        return taxi;
    }

    public void setTaxi(Boolean taxi) {
        this.taxi = taxi;
    }

    public String getPaymentPurpose() {
        return paymentPurpose;
    }

    public void setPaymentPurpose(String paymentPurpose) {
        this.paymentPurpose = paymentPurpose;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getInsurerAccount() {
        return insurerAccount;
    }

    public void setInsurerAccount(String insurerAccount) {
        this.insurerAccount = insurerAccount;
    }

    public String getStickerNumber() {
        return stickerNumber;
    }

    public void setStickerNumber(String stickerNumber) {
        this.stickerNumber = stickerNumber;
    }

    @Override
    public String toString() {
        return "EwaContract{" +
                "type='" + type + '\'' +
                ", id=" + id +
                ", code='" + code + '\'' +
                ", salePoint=" + salePoint +
                ", user=" + user +
                ", created='" + created + '\'' +
                ", stateChanged='" + stateChanged + '\'' +
                ", stateChangedBy=" + stateChangedBy +
                ", tariff=" + tariff +
                ", number='" + number + '\'' +
                ", date='" + date + '\'' +
                ", dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", period=" + period +
                ", customer=" + customer +
                ", beneficiary=" + beneficiary +
                ", insuranceObject=" + insuranceObject +
                ", insuranceObjects='" + insuranceObjects + '\'' +
                ", baseTariff=" + baseTariff +
                ", discount=" + discount +
                ", urgencyCoefficient=" + urgencyCoefficient +
                ", payment=" + payment +
                ", brokerDiscountedPayment=" + brokerDiscountedPayment +
                ", agents='" + agents + '\'' +
                ", notes='" + notes + '\'' +
                ", state='" + state + '\'' +
                ", payments=" + payments +
                ", risks='" + risks + '\'' +
                ", customFields='" + customFields + '\'' +
                ", multiObject=" + multiObject +
                ", signerOtpState='" + signerOtpState + '\'' +
                ", customerOtpState='" + customerOtpState + '\'' +
                ", usageMonths=" + usageMonths +
                ", franchise=" + franchise +
                ", k1=" + k1 +
                ", k2=" + k2 +
                ", k3=" + k3 +
                ", k4=" + k4 +
                ", k5=" + k5 +
                ", k6=" + k6 +
                ", bonusMalus=" + bonusMalus +
                ", privilege=" + privilege +
                ", privilegeType='" + privilegeType + '\'' +
                ", drivingExpLessThreeYears=" + drivingExpLessThreeYears +
                ", taxi=" + taxi +
                ", paymentPurpose='" + paymentPurpose + '\'' +
                ", commission='" + commission + '\'' +
                ", insurerAccount='" + insurerAccount + '\'' +
                ", stickerNumber='" + stickerNumber + '\'' +
                '}';
    }
}
