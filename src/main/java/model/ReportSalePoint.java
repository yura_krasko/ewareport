package model;

import java.io.Serializable;

public class ReportSalePoint implements Serializable {

    private static final long serialVersionUID = 12111532466L;

    private String code;
    private String name;

    public ReportSalePoint() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
