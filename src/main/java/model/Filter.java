package model;

import java.io.Serializable;
import java.util.List;

public class Filter implements Serializable{

    private static final long serialVersionUID = 12345566L;

    private List<ParameterFiltr> filters;
    private Integer limit;
    private Integer offset;

    public List<ParameterFiltr> getFilters() {
        return filters;
    }

    public void setFilters(List<ParameterFiltr> filters) {
        this.filters = filters;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
