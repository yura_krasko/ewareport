package mail;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import util.DateOfSelect;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Properties;

public class Email {

    private static Logger logger = Logger.getLogger(Email.class.getName());

    DateOfSelect selectDate = new DateOfSelect();

    /**
     * Send email
     */
    public void sendEmail()  {

        Properties properties = new Properties();

        try {
            properties.load(Email.class.getClassLoader().getResourceAsStream("connection.properties"));
            String userName = properties.getProperty("mail.smtp.user");
            String host = properties.getProperty("mail.smtp.host");
            String userPassword = properties.getProperty("mail.smtp.password");
            String users = properties.getProperty("mail_user");

            Session session = Session.getDefaultInstance(properties);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(userName));

            message.addRecipients(Message.RecipientType.TO, users);
            message.setSubject("Отчет о продажах Юниверсал Ассистанс за " + selectDate.getSelectDate());

            //send attachment
            BodyPart messageFilePart = new MimeBodyPart();
            String filename = "report.xlsx";
            String fileloc = properties.getProperty("PATH_TO_FILE");

            DataSource source = new FileDataSource(fileloc);
            messageFilePart.setDataHandler(new DataHandler(source));
            messageFilePart.setFileName(filename);
            Multipart  multipart = new MimeMultipart();

            multipart.addBodyPart(messageFilePart);
            message.setContent(multipart);

            logger.info("Sending...");

            Transport transport = session.getTransport();
            transport.connect(host, userName, userPassword);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            logger.info("The letter has been successfully sent!");

        } catch (IOException e) {
            logger.log(Level.ALL, "Exseption", e);
        } catch (AddressException e) {
            logger.log(Level.ALL, "Exseption", e);
        } catch (NoSuchProviderException e) {
            logger.log(Level.ALL, "Exseption", e);
        } catch (MessagingException e) {
            logger.log(Level.ALL, "Exseption", e);
        }
    }
}
