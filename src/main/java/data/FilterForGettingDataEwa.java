package data;

import model.Filter;
import model.ParameterFiltr;

import java.util.ArrayList;
import java.util.List;

public class FilterForGettingDataEwa {

    private Filter filter;
    private List<ParameterFiltr> list;
    private ParameterFiltr parameterFiltr;

    /**
     * Add data to filter
     * @param productId id products.
     * @param selectDate date of selection.
     * @return
     */
    public Filter createFilter(Integer productId, String selectDate){
        filter = new Filter();
        list = new ArrayList<ParameterFiltr>();
        list.add(createParameterFiltr("date", "after", "date", selectDate));
        list.add(createParameterFiltr("date", "before", "date", selectDate));
        list.add(createParameterFiltr("state", "in", "list", "SIGNED"));
        list.add(createParameterFiltr("tariff.id", "eq", "numeric", String.valueOf(productId)));
        filter.setFilters(list);
        filter.setLimit(10000);
        filter.setOffset(0);
        return filter;
    }


    /**
     * Create parameters of filter
     */
    private ParameterFiltr createParameterFiltr(String field, String comparison, String type, String value){
        parameterFiltr = new ParameterFiltr();
        parameterFiltr.setField(field);
        parameterFiltr.setComparison(comparison);
        parameterFiltr.setType(type);
        parameterFiltr.setValue(value);
        return parameterFiltr;
    }
}






