package data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import connection.ConnectionEwa;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import util.DateOfSelect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EwaData {

    private final static Logger LOGGER = Logger.getLogger(EwaData.class.getName());

    private final DateOfSelect dateOfSelect= new DateOfSelect();
    private String baseUrl;
    private Properties properties;
    private String  requestEwa;
    private List<String> rezult;
    private FilterForGettingDataEwa dataFilter;
    private final Integer[] idProduct = {9702, 9705, 9598, 9600, 9707, 9706, 10225};

    /**
     *Initialization variable
     */
    public EwaData() {
        try {
            properties = new Properties();
            properties.load(ConnectionEwa.class.getClassLoader().getResourceAsStream("connection.properties"));
            baseUrl = properties.getProperty("EWA_BASE_URL");
        } catch (IOException e) {
            LOGGER.log(Level.ALL, "Exception", e);
        }
    }

    /**
     * Get contract from EWA
     *
     * @param cookie
     * @return
     */
    public List<String> getContract(String cookie){
        final String BASE_URL = baseUrl + "contract?simple=false";
        rezult = new ArrayList<String>();
        Gson gson = new GsonBuilder().create();
        dataFilter = new FilterForGettingDataEwa();
        for (int i = 0; i <idProduct.length ; i++) {
            String filter = gson.toJson(dataFilter.createFilter(idProduct[i], dateOfSelect.getSelectDate()));
            try {
                URL url = new URL(BASE_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestProperty("Cookie", cookie);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                OutputStream out = connection.getOutputStream();
                out.write(filter.getBytes("UTF-8"));
                out.close();

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine = null;

                StringBuffer buffer = new StringBuffer();
                while ((inputLine = reader.readLine()) != null) {
                    requestEwa = buffer.append(inputLine).toString();
                }
                rezult.add(requestEwa);
                LOGGER.info("Get data from Ewa!");
            } catch (IOException e) {
                LOGGER.log(Level.ALL, "Exception", e);
            }
        }
        return rezult;
    }
}
