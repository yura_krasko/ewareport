package connection;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class LogoutEwa {

    private final static Logger LOGGER = Logger.getLogger(LogoutEwa.class.getName());

    private String baseUrl;
    private Properties properties;
    private URL myUrl;
    private HttpURLConnection connection;
    /**
     * Initialization variable
     */
    public LogoutEwa() {
        try {
            properties = new Properties();
            properties.load(ConnectionEwa.class.getClassLoader().getResourceAsStream("connection.properties"));
            baseUrl = properties.getProperty("EWA_BASE_URL");
        } catch (IOException e) {
            LOGGER.log(Level.ALL, "Exseption", e);
        }
    }

    /**
     * Logout Ewa
     */
    public void lohoutEwa() {
        String url = baseUrl + "user/logout";
        try {
            myUrl = new URL(url);
            connection = (HttpURLConnection) myUrl.openConnection();
            connection.setRequestMethod("PUT");
            LOGGER.info("Logout Ewa");
        } catch (IOException e) {
            LOGGER.log(Level.ALL, "Exseption", e);
        }
    }
}
