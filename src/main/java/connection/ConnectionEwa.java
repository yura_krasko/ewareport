package connection;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;


public class ConnectionEwa {

    private final static Logger LOGGER = Logger.getLogger(ConnectionEwa.class.getName());

    private String userName;
    private String userPassword;
    private String baseUrl;
    private String cookie;
    private HttpURLConnection connection;
    private Properties properties;

    /**
     * Initialization variable
     */
    public ConnectionEwa() {
        try {
            properties = new Properties();
            properties.load(ConnectionEwa.class.getClassLoader().getResourceAsStream("connection.properties"));
            userName= properties.getProperty("EWA_USER_NAME");
            userPassword = properties.getProperty("EWA_USER_PASSWORD");
            baseUrl = properties.getProperty("EWA_BASE_URL");
        } catch (IOException e) {
            LOGGER.log(Level.ALL, "Exseption", e);
        }
    }

    /**
     * Connection to EWA and get cookie     *
     * @return cookie
     */
    public String getConnection() {
        String url = baseUrl;
        try {
            LOGGER.info("Connection...");
            URL myUrl = new URL(url);
            connection = (HttpURLConnection) myUrl.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("email", userName);
            connection.setRequestProperty("password", userPassword);
            LOGGER.info("Connection successfull!");
            cookie = connection.getHeaderField("Set-Cookie");
        } catch (IOException e) {
            LOGGER.log(Level.ALL, "Exseption", e);
        } finally {
            connection.disconnect();
        }
        return cookie;
    }
}
