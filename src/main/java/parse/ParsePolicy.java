package parse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import connection.ConnectionEwa;
import model.Contract;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import poi.StyleReport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class ParsePolicy {

    private static Logger logger = Logger.getLogger(ParsePolicy.class.getName());

    String pathToFile;
    StyleReport styleReport = new StyleReport();

    public ParsePolicy() {
        try {
            Properties properties = new Properties();
            properties.load(ConnectionEwa.class.getClassLoader().getResourceAsStream("connection.properties"));
            pathToFile = properties.getProperty("PATH_TO_FILE");
        } catch (IOException e) {
            logger.log(Level.ALL, "Exseption", e);
        }
    }

    /**
     * Parse respons from Ewa and write to xlsx file
     *
     * @param json
     */
    public void paresJson(List<String> json){

       Gson gson = new GsonBuilder().create();
       //number of row
       int rowNumber= 0;
       //create xlsx
        XSSFWorkbook workbook = new XSSFWorkbook();
        //create sheet
        XSSFSheet sheet = workbook.createSheet("Договоры ОСАГО");
        //create row 0
        Row rowHeader = sheet.createRow(rowNumber);
        rowHeader.setHeightInPoints(30);
        sheet.setColumnWidth(0, 5000);
        styleReport.createCellHeader(workbook, rowHeader, (short)0, "ОСЦВ" );
        styleReport.createCellHeader(workbook, rowHeader, (short)1, "Дата укладання" );
        styleReport.createCellHeader(workbook, rowHeader, (short)2, "Дата вступу" );
        styleReport.createCellHeader(workbook, rowHeader, (short)3, "Дата закінчення дії" );
        styleReport.createCellHeader(workbook, rowHeader, (short)4, "ПІБ / Найменування страхувальника" );
        styleReport.createCellHeader(workbook, rowHeader, (short)5, "Марка та модель ТЗ" );
        styleReport.createCellHeader(workbook, rowHeader, (short)6, "Рік випуску" );
        styleReport.createCellHeader(workbook, rowHeader, (short)7, "Держ. номер" );
        styleReport.createCellHeader(workbook, rowHeader, (short)8, "VIN" );
        styleReport.createCellHeader(workbook, rowHeader, (short)9, "Платеж" );
        styleReport.createCellHeader(workbook, rowHeader, (short)10, "Категория" );
        styleReport.createCellHeader(workbook, rowHeader, (short)11, "ID продукта" );
        styleReport.createCellHeader(workbook, rowHeader, (short)12, "Название продукта" );

        for (int j = 0; j < json.size(); j++) {
        Contract []contracts  = gson.fromJson(json.get(j), Contract[].class);
        logger.info("Write a file...");
        for (Contract contract : contracts) {
            //Conditions of select
           if ((contract.getBrokerDiscountedPayment() >= 405 && !contract.getInsuranceObject().getCategory().equals("F") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                   || (contract.getBrokerDiscountedPayment() >= 405 && !contract.getInsuranceObject().getCategory().equals("E") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                   || (contract.getBrokerDiscountedPayment() >= 405 && !contract.getInsuranceObject().getCategory().equals("A1") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                   || (contract.getBrokerDiscountedPayment() >= 405 && !contract.getInsuranceObject().getCategory().equals("A2") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                   ) {
                ++rowNumber;
                Row dataRow = sheet.createRow(rowNumber);
                sheet.setColumnWidth(rowNumber, 7500);
               styleReport.createCell(workbook, dataRow, (short) 0, contract.getNumber());
               styleReport.createCell(workbook, dataRow, (short) 1, dateFormat(contract.getDate()));
               styleReport.createCell(workbook, dataRow, (short) 2, dateFormat(contract.getDateFrom()));
               styleReport.createCell(workbook, dataRow, (short) 3, dateFormat(contract.getDateTo()));
               styleReport.createCell(workbook, dataRow, (short) 4, contract.getCustomer().getName());
               styleReport.createCell(workbook, dataRow, (short) 5, contract.getInsuranceObject().getModelText());
               styleReport.createCell(workbook, dataRow, (short) 6, contract.getInsuranceObject().getYear().toString());
               styleReport.createCell(workbook, dataRow, (short) 7, contract.getInsuranceObject().getStateNumber());
               styleReport.createCell(workbook, dataRow, (short) 8, contract.getInsuranceObject().getBodyNumber());
               styleReport.createCell(workbook, dataRow, (short) 9, contract.getBrokerDiscountedPayment().toString());
               styleReport.createCell(workbook, dataRow, (short) 10, contract.getInsuranceObject().getCategory());
               styleReport.createCell(workbook, dataRow, (short) 11, contract.getTariff().getId().toString());
               styleReport.createCell(workbook, dataRow, (short) 12, contract.getTariff().getName());
                }
            }
        }
        //write xlsx into file
        try(FileOutputStream outputStream = new FileOutputStream(new File(pathToFile))) {
            workbook.write(outputStream);
            outputStream.flush();
            logger.info("File saved succesfull!");
        } catch (IOException e) {
            logger.log(Level.ALL, "Exseption", e);
        }
    }

    /**
     * Format date 'dd.MM.yyyy'
     *
     * @param date
     * @return
     */
    private String dateFormat(String date){
        SimpleDateFormat dateFormatOld = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormatNew = new SimpleDateFormat("dd.MM.yyyy");
        Date dateParse = null;
        try {
            dateParse = dateFormatOld.parse(date);
        } catch (ParseException e) {
            logger.log(Level.ALL, "Exseption", e);
        }
        return dateFormatNew.format(dateParse);
    }
}
