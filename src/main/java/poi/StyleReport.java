package poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;

public class StyleReport {

    /**
     * Create cell header style
     *
     * @param workbook
     * @param row
     * @param column
     * @param nameColumn
     */
    public void createCellHeader(Workbook workbook, Row row, Short column, String nameColumn) {

        XSSFFont font= (XSSFFont) workbook.createFont();
        font.setFontHeightInPoints((short)11);
        font.setFontName("Arial");
        font.setBold(true);
        font.setItalic(true);

        XSSFCell cell = (XSSFCell) row.createCell(column);
        cell.setCellValue(nameColumn);
        XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setFont(font);
        cellStyle.setBorderRight((short) 2);
        cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(193, 193, 199)));
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyle.setBorderLeft((short) 2);
        cellStyle.setBorderTop((short) 2);
        cellStyle.setBorderBottom((short) 2);
        cell.setCellStyle(cellStyle);
    }


    /**
     * Create cell style
     */

    public void createCell(Workbook workbook, Row row, Short column, String nameColumn) {

        XSSFFont font= (XSSFFont) workbook.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");

        Cell cell = row.createCell(column);
        cell.setCellValue(nameColumn);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
        cellStyle.setFont(font);
        cellStyle.setBorderRight((short) 1);
        cellStyle.setBorderLeft((short) 1);
        cellStyle.setBorderTop((short) 1);
        cellStyle.setBorderBottom((short) 1);
        cell.setCellStyle(cellStyle);
    }
}
