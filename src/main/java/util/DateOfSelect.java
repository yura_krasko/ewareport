package util;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateOfSelect {

    private final static Logger LOGGER = Logger.getLogger(DateOfSelect.class);

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Date date;
    private Calendar calendar;
    private String dateString;
    private Long unixTime = null;

    /**
     * Converting date to format timestamp(now not used).
     * @return date to farmat timestamp.
     */
    public Long getDateToTimestamp(){
        try {
            calendar= Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            dateString = dateFormat.format(calendar.getTime());
            date = dateFormat.parse(dateString);
            unixTime = (long)date.getTime();
        } catch (ParseException e) {
            LOGGER.debug("Exception", e);
        }
        return unixTime;
    }

    /**
     * Date of selection data from EWA
     * @return date to format yyyy-MM-dd.
     */
    public String getSelectDate(){
        final String selectDate;
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        selectDate = dateFormat.format(calendar.getTime());
        return selectDate;
    }
}
