import connection.ConnectionEwa;
import connection.LogoutEwa;
import data.EwaData;
import mail.Email;
import parse.ParsePolicy;

public class Main {

    public static void main(String[] args) {

        Email email = new Email();
        ConnectionEwa connectionEwa = new ConnectionEwa();
        EwaData data = new EwaData();
        ParsePolicy parsePolicy = new ParsePolicy();
        LogoutEwa logoutEwa  =new LogoutEwa();
        parsePolicy.paresJson(data.getContract(connectionEwa.getConnection()));
        email.sendEmail();
        logoutEwa.lohoutEwa();
    }
}
